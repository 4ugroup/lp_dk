$('.clients').slick({
    arrows: true,
    autoplay: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="clients-nav prev"><i class="fa fa-long-arrow-left"></i></span>',
    nextArrow: '<span class="clients-nav next"><i class="fa fa-long-arrow-right"></i></span>'
});


$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn_close" href="javascript:;"><i class="fa fa-close"></i></a>'
    }
});


$(window).load(function() {



});


$(document).ready(function() {

    // Анимация
    var Android = navigator.userAgent.search(/Android/i);
    var iPhone = navigator.userAgent.search(/iPhone/i);
    var iPad = navigator.userAgent.search(/iPad/i);
    if(Android != -1 || iPhone != -1 || iPad != -1) {

        $('head').append('<link rel="stylesheet" type="text/css" href="css/mobile.css" />'); //подключение файла mobile.css если мобильник

    } else {


        $('#fullpage').fullpage({
            responsiveWidth: 1230, // Ширина
            responsiveHeight: 700 // Высота
        });

    }


});